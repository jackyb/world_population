World Population
================

This software simulates population number and sends the number to serial port
(e.g. Arduino). It allows the user to input base population number and rate of
change. It automatically saves and loads data upon closing and opening.

It uses the first serial port that it finds so on Windows, it might be required
to uninstall the rest of the communication ports on the Device Manager. You can
do "Scan for hardware changes" to regain the ports.
