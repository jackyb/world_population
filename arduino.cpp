#include "arduino.h"

#include <QSerialPortInfo>
#include <QIODevice>
#include <QList>
#include <QString>
#include <QDebug>

ArduinoWorker::ArduinoWorker(QObject *parent) :
	QObject(parent),
	port(0)
{
	openPort();
}

ArduinoWorker::~ArduinoWorker() {
	closePort();
}

QSerialPort *ArduinoWorker::serialPort() const {
	return port;
}

void ArduinoWorker::onError(QSerialPort::SerialPortError error) {
	// Handle when the cable is unplugged while the program is running
	if (error == QSerialPort::ResourceError) {
		port = 0;
		// FIXME: Possible memory leak. Strangely, deleting port will crash the program.
	}
}

bool ArduinoWorker::openPort(const QString &name) {
	closePort();

	auto ports = QSerialPortInfo::availablePorts();
	if (ports.size() == 0) {
		qDebug() << "No serial port is available";
		return false;
	}

	auto idx = 0;
	for (int i = 0; i < ports.size(); i++) {
		if (ports[i].portName().contains(name)) {
			idx = i;
			break;
		}
	}
	auto info = QSerialPortInfo::availablePorts()[idx];

	port = new QSerialPort(info);
	port->setBaudRate(QSerialPort::Baud9600);
	if (!port->open(QIODevice::ReadWrite)) {
		port = 0;
		qDebug() << "Failed to open serial port: " << info.portName();
		return false;
	}
	qDebug() << "Opened port: " << info.portName();

	connect(port, SIGNAL(error(QSerialPort::SerialPortError)),
		this, SLOT(onError(QSerialPort::SerialPortError)));
	

	return true;
}

void ArduinoWorker::closePort() {
	if (!port) {
		return;
	}

	port->close();
	qDebug() << "Closed port";
	delete port;
}

void ArduinoWorker::updatePopulation(double value) {
	// Try to get serial port if there's currently none available
	if (!port) {
		if (!openPort()) {
			return;
		}
	}

	// Write data to Arduino
	if (!sendPopulationData(value))
		return;
	
	// Wait for data to be written
	auto ok = port->waitForBytesWritten(1000);
	if (!ok) {
		qDebug() << "Write timed out";
		return;
	}
}

bool ArduinoWorker::sendPopulationData(double population) {
	char buf[10] = { 0 };

	buf[0] = 0xDB;
	buf[1] = 0xBD;
	quint64 *p = (quint64 *) &buf[2];
	*p = (quint64) population;

	auto n = port->write(buf, 10);
	if (n <= 0) {
		qDebug() << "Failed to write data to Arduino";
		return false;
	}

	return true;
}

ArduinoController::ArduinoController(QObject *parent) :
	QObject(parent)
{
	worker = new ArduinoWorker(this);
	worker->moveToThread(&workerThread);

	connect(this, SIGNAL(updatePopulation(double)), worker, SLOT(updatePopulation(double)));
	connect(&workerThread, SIGNAL(finished()), worker, SLOT(deleteLater()));

	workerThread.start();
}

ArduinoController::~ArduinoController() {
	workerThread.quit();
	workerThread.wait();
}

bool ArduinoController::openPort(const QString &name) {
	return worker->openPort(name);
}

QSerialPort *ArduinoController::serialPort() const {
	return worker->serialPort();
}
