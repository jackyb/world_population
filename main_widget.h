#ifndef MAIN_WIDGET_H
#define MAIN_WIDGET_H

#include <QWidget>

class ArduinoController;

class QTimer;
class QLabel;
class QPushButton;
class QLineEdit;
class QCheckBox;

class MainWidget : public QWidget {
	Q_OBJECT

public:
	MainWidget(QWidget *parent = 0, Qt::WindowFlags flags = 0);

	bool openPort(const QString &portName);
	const QString portName() const;

public slots:
	void save();
	void load();

private slots:
	void updatePopulation();
	void setPopulationToBase();

private:
	const int MAX_UPDATE_RATE = 10;
	
	double population;
	double base;
	double rate;

	QLabel *populationLabel;
	QLineEdit *baseEdit;
	QLineEdit *rateEdit;

	ArduinoController *controller;
	QTimer *timer;
	int updateRate;

	void setupUi();
	void adjustUpdateRate();
};

#endif
