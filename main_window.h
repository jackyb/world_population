#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QMainWindow>

class QWidget;
class QLabel;
class QMenu;
class MainWidget;

class MainWindow : public QMainWindow {
	Q_OBJECT

public:	
	MainWindow(QWidget *parent = 0, Qt::WindowFlags flags = 0);
	QLabel *getStatusLabel() const;

private:
	MainWidget *widget;
	QLabel *statusLabel;
	QMenu *portMenu;

	void setupMenu();

private slots:
	bool openPort(QAction *);
	void refreshPortList();

private:
	void addPort(const QString &);
	const QString portName() const;
};

#endif
