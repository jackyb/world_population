#include "main_widget.h"
#include "main_window.h"
#include "arduino.h"

#include <QMainWindow>
#include <QLabel>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QDoubleValidator>
#include <QTimer>
#include <QFile>
#include <QIODevice>
#include <QDataStream>
#include <QDateTime>
#include <QDebug>

#include <cfloat>

MainWidget::MainWidget(QWidget *parent, Qt::WindowFlags flags) :
	QWidget(parent, flags),
	population(0),
	base(0),
	rate(0)
{
	// Set UI
	setupUi();

	// Set Arduino controller
	controller = new ArduinoController(this);

	// Set save on exit
	connect(parent, SIGNAL(destroyed()), this, SLOT(save()));

	// Load data
	load();

	// Set update timer
	timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(updatePopulation()));

	adjustUpdateRate();
	timer->start();
}

bool MainWidget::openPort(const QString &name) {
	return controller->openPort(name);
}

void MainWidget::save() {
	QFile file("settings.bin");
	if (!file.open(QIODevice::WriteOnly)) {
		qDebug() << "save(): Could not create file for saving settings.";
		return;
	}

	QSerialPort *port = controller->serialPort();
	QDataStream out(&file);
	out.setVersion(QDataStream::Qt_5_3);
	if (port) {
		out << population << base << rate << port->portName() << QDateTime::currentDateTime();
	} else {
		out << population << base << rate << QString("none") << QDateTime::currentDateTime();
	}
}

void MainWidget::load() {
	QFile file("settings.bin");
	if (!file.open(QIODevice::ReadOnly)) {
		qDebug() << "load(): Could not open file for loading settings.";
		return;
	}

	QDateTime prevDateTime;
	QString portName;
	QDataStream in(&file);
	in.setVersion(QDataStream::Qt_5_3);
	in >> population >> base >> rate >> portName >> prevDateTime;

	int secs = prevDateTime.secsTo(QDateTime::currentDateTime());
	population += rate * secs;
	populationLabel->setText(QString::number(population).setNum(population, 'f', 2));
	baseEdit->setText(QString::number(base));
	rateEdit->setText(QString::number(rate));
	openPort(portName);
}

void MainWidget::updatePopulation() {
	base = baseEdit->text().toDouble();
	rate = rateEdit->text().toDouble();
	adjustUpdateRate();

	population += rate / updateRate;
	populationLabel->setText(QString::number(population).setNum(population, 'f', 2));
	controller->updatePopulation(population);
}

void MainWidget::setPopulationToBase() {
	population = baseEdit->text().toDouble();
	populationLabel->setText(QString::number(population).setNum(population, 'f', 2));
}

void MainWidget::setupUi() {
	// Create root layout
	auto layout = new QVBoxLayout(this);
	setLayout(layout);

	// Create population label
	populationLabel = new QLabel(QString::number(0), this);
	layout->addWidget(populationLabel);

	// Create layout 1
	auto layout1 = new QHBoxLayout(this);
	layout->addLayout(layout1);

	// Create base LineEdit
	baseEdit = new QLineEdit(this);
	baseEdit->setValidator(new QDoubleValidator(0, DBL_MAX, 0, this));
	layout1->addWidget(baseEdit);

	// Create rate LineEdit
	rateEdit = new QLineEdit(this);
	rateEdit->setValidator(new QDoubleValidator(0, DBL_MAX, 2, this));
	layout1->addWidget(rateEdit);

	// Create layout 2
	auto layout2 = new QHBoxLayout(this);
	layout->addLayout(layout2);

	// Create reset population button 
	auto button = new QPushButton(this);
	button->setText(tr("Set Population to Base"));
	connect(button, SIGNAL(clicked()), this, SLOT(setPopulationToBase()));
	layout2->addWidget(button);
}

void MainWidget::adjustUpdateRate() {
	if (rate >= 0.25) {
		updateRate = rate * 4;
		if (updateRate > MAX_UPDATE_RATE) {
			updateRate = MAX_UPDATE_RATE;
		}
	} else {
		updateRate = 1;
	}

	timer->setInterval(1000 / updateRate);
}

const QString MainWidget::portName() const {
	return controller->serialPort()->portName();
}
