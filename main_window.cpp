#include "main_window.h"
#include "main_widget.h"

#include <QWidget>
#include <QMenuBar>
#include <QMenu>
#include <QAction>
#include <QList>
#include <QSerialPortInfo>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent, Qt::WindowFlags flags) :
	QMainWindow(parent, flags)
{
	widget = new MainWidget(this);
	setCentralWidget(widget);

	setupMenu();
}

QLabel *MainWindow::getStatusLabel() const {
	return statusLabel;
}

void MainWindow::setupMenu() {
	// Set port menu
	portMenu = menuBar()->addMenu(tr("&Port"));
	connect(portMenu, SIGNAL(triggered(QAction *)), this, SLOT(openPort(QAction *)));
	connect(portMenu, SIGNAL(aboutToShow()), this, SLOT(refreshPortList()));
}

bool MainWindow::openPort(QAction *action) {
	return widget->openPort(action->text());
}

void MainWindow::refreshPortList() {
	portMenu->clear();

	auto portInfos = QSerialPortInfo::availablePorts();
	foreach (auto info, portInfos) {
		addPort(info.portName());
	}
}

void MainWindow::addPort(const QString &name) {
	auto action = new QAction(name, this);
	portMenu->addAction(action);
	if (portName().compare(name) == 0) {
		auto font = action->font();
		font.setBold(true);
		action->setFont(font);
	}
}

const QString MainWindow::portName() const {
	return widget->portName();
}
