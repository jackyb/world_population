#ifndef ARDUINO_H
#define ARDUINO_H

#include <QSerialPort>
#include <QThread>

class QSerialPort;
class QString;

/*****************
 * ArduinoWorker *
 *****************/
class ArduinoWorker : public QObject {
	Q_OBJECT

public:
	ArduinoWorker(QObject *parent = 0);
	~ArduinoWorker();

	bool openPort(const QString &name = 0);

	QSerialPort *serialPort() const;

public slots:
	void updatePopulation(double);

private slots:
	void onError(QSerialPort::SerialPortError);

private:
	QSerialPort *port;

	void closePort();
	bool sendPopulationData(double);
};

/*********************
 * ArduinoController *
 *********************/
class ArduinoController : public QObject {
	Q_OBJECT

public:
	ArduinoController(QObject *parent = 0);
	~ArduinoController();

	bool openPort(const QString &name = 0);

	QSerialPort *serialPort() const;

signals:
	void updatePopulation(double);

private:
	ArduinoWorker *worker;
	QThread workerThread;
};

#endif
